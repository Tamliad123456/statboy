import pefile
import os
from pprint import pprint
import address
import struct
from binarytree import Node

file_path = r"ConsoleApplication1.exe"


def getImportAddresses(file_path):
    pe = pefile.PE(file_path)

    nameToAddr = {}

    for entry in pe.DIRECTORY_ENTRY_IMPORT:
        for func in entry.imports:
            nameToAddr[int(str("%08x" % func.address).upper(),
                           base=16)] = func.name.decode('utf-8')

    return nameToAddr


def getEntryPoint(file_path):
    pe = pefile.PE(file_path)
    return (pe.OPTIONAL_HEADER.AddressOfEntryPoint + pe.OPTIONAL_HEADER.ImageBase)


def getAssemblyFromFile(file_path):

    data = os.popen(f"dumpbin /DISASM:NOBYTES /NOPDB {file_path}").read()

    return data


def getValuesFromLine(line):
    calls = ['call', 'jmp', 'jnz', 'je', 'jne', 'jg', 'jge', 'ja', 'jae', 'jl', 'jle',
             'jb', 'jbe', 'jo', 'jno', 'jz', 'jnz', 'js', 'jns', 'jcxz', 'jecxz', 'jrcxz']
    rets = ['ret', 'retn']
    splitted = line.split(':')
    
    addr, command = splitted[0].strip(), splitted[1]

    splitValues = [x.strip() for x in command.split(' ') if x != ""]
    opcode, value = splitValues[0], " ".join(splitValues[1:])
    return (int(addr, base=16), address.Command(int(addr, base=16), opcode, value, opcode in calls, opcode in rets))


def getCodes(data):

    data = data.split('\n')[8:-13]

    iterator = filter(lambda line: line != "", data)
    for line in iterator:
        yield getValuesFromLine(line)


def listToDict(toupleList):
    return dict(toupleList)


def makeList(tree):
    paths = []
    if not tree or tree.visited:
        return [paths]

    tree.visited = True

    path = [[int(address.cleanValue(x.val), base=16) for x in tree.external_commands] + child for child in makeList(tree.left)]
    paths += filter(lambda p: p not in paths, path)

    path = [[int(address.cleanValue(x.val), base=16) for x in tree.external_commands] + child for child in makeList(tree.right)]
    paths += filter(lambda p: p not in paths, path)

    return paths



def addKnownFunctions(paths, nameToAddr):
    newPaths = []

    for path in paths:
        newPath = []

        for value in path:
            if value in nameToAddr:
                newPath.append(nameToAddr[value])

        newPaths.append(newPath)

    return newPaths

def removeDupl(lists):
    
    newList = []
    
    for l1i, list1 in enumerate(lists):
        
        valid = True
        for i, list2 in enumerate(lists):
            if all(elem in list2  for elem in list1) and i != l1i:
                valid = False
                break

        if(valid): newList.append(list1)

    return newList
                


def printTree(root, depth=0):

    if not root or root.visited:
        return

    print((" " * depth), ', '.join([nameToAddr.get(int(address.cleanValue(
        x.val), base=16), "clum") for x in root.external_commands]))

    root.visited = True
    printTree(root.left, depth + 1)
    printTree(root.right, depth + 1)


if __name__ == "__main__":
    entryPoint = getEntryPoint(file_path)
    nameToAddr = getImportAddresses(file_path)
    data = getAssemblyFromFile(file_path)
    dataDict = listToDict(getCodes(data))
    data_keys = sorted(dataDict.keys())
    root = address.runOnTree(dataDict, entryPoint, {}, data_keys, nameToAddr.keys())
    #print("yay")
    #print(root)
    printTree(root)
    #paths = makeList(root)
    ###print(paths)
    #newPaths = addKnownFunctions(paths, nameToAddr)
    #print(removeDupl(newPaths))
