import re
import os
import sys
from pprint import pprint
import mmap
import json

funcNames = None

def parseFile(path):
    if os.path.exists("output.gdl"):
        newContent = open(path, "r", encoding="utf-8").readlines()
    else: return (None, None)
    content = []
    for  line in newContent:
        if("}" in line and ';' in line):
            line = re.sub(r';(.+)', '" }', line)
        elif ';' in line:
            line = re.sub(r';(.+)', '', line)
        content.append(line)
    content = ''.join(content)
    nodes_regex = r"node: ({[\S\s]*?})"
    edges_regex = r"edge: ({[\S\s]*?})"
    
    nodes = re.findall(nodes_regex, content)
    edges = re.findall(edges_regex, content)
    
    return (nodes, edges)
    
def handleNode(node):
    header_regex = r"([a-zA-Z0-9-]+): \"([\S\s]*?)\""
    
    res = re.findall(header_regex, node)
    title = res[0][1]
    content = res[1][1]
    
    return (title, content.split("\n"))

#C:\Program Files\IDA 7.2\cfg\ida.cfg
#ABANDON_DATABASE = YES

def buildNodes(nodes):
    global funcNames
    ret = dict()
    for node in nodes:
        title, content = handleNode(node)
        reList = [re.findall(r"(call.+)(cs:|ds:| )(.+)", x.split(";")[0]) for x in filter(lambda x: 'call ' in x, content)]
        content = [funcNames.get(call[0][2],-1) for call in reList if len(call) != 0]
        while (content.count(-1)): content.remove(-1)
        
        #content = [funcNames.get(re.findall(r"(call.+)(cs:|ds:| )(.+)", x.split(";")[0])[0][2].strip(), -1) for x in filter(lambda x: 'call ' in x, content)] 
        ret[title] = {"content" : content}

    return ret

def handleEdge(edge):
    header_regex = r"([a-zA-Z0-9-]+): \"([\S\s]*?)\""
    src = re.findall(header_regex, edge)[0][1]
    dst = re.findall(header_regex, edge)[1][1]
    return (src, dst)

def buildEdges(edges):
    ret = dict()
    
    for edge in edges:
        src, dst = handleEdge(edge)
        l = ret.get(src, False)
        if not l:
            ret[src] = list()
        ret[src].append(dst)
        
    return ret



def buildFlow(out_path):
    nodes, edges = parseFile(out_path)
    if not nodes and not edges:
        return (None, None)
    return (buildNodes(nodes), buildEdges(edges))
   
   
def getFlow(path):
    idaPath = r'"C:\Program Files\IDA 7.2\ida64.exe"'
    os.system(f'start /wait "idaPro" {idaPath} -A -S"C:\\Users\\magshimim\\Desktop\\statboy\\idaScript.py" {path}')
    return buildFlow("output.gdl")


def runOnTree(value, nodes, edges, currPath, final, depth = 0):
    currNode = nodes[value]
    arr = currPath[:]
    arr += currNode["content"]
    if depth == 4 or not value in edges:
        if arr not in final:
            final.append(arr)
        return currPath
    else:
        for jump in edges[value]:
            lst = runOnTree(jump, nodes, edges, arr, final, depth + 1)
        return currPath


def runOnEdges(nodes, edges):
    final = list()
    if len(edges) <= 4:
        arr = runOnTree("0", nodes, edges, list(), final)
        return final
    else:
        for edge in edges:
            arr = runOnTree(edge, nodes, edges,list(), final)
        return final


def dumpToFile(arr):
    jsonData = None
    with open('learn.txt', 'r+') as outfile:
        jsonData = json.load(outfile)
        jsonData.append(arr)
        outfile.seek(0)
        json.dump(jsonData, outfile)
        outfile.truncate()  
        


def mainFunc(filePath = ""):
    global funcNames
    with open("Data.txt" , "r+") as funcNamesFile:
        funcNames = enumerate(funcNamesFile.read().split("\n"))
    funcNames = dict(map(lambda x: (x[1], x[0]) ,funcNames))
    #f = [x.replace('\n', '') for x in open("Data.txt" , "r").readlines()]
    #windict = dict(enumerate(f))
    if len(sys.argv) >= 2:
        filePath = sys.argv[1]
    #filePath = r"malwares\apt\Win32.SofacyCarberp.exe"
    #filePath = "ConsoleApplication1.exe"
    nodes, edges = getFlow(filePath)
    if nodes is None and edges is None:
        return []
    #paths = runOnEdges(nodes , edges)
    #pprint(paths)
    os.remove("output.gdl")
    return runOnEdges(nodes , edges)
    #biggest(max(len(paths)) , paths)



if __name__ == "__main__":
    print(mainFunc())    
