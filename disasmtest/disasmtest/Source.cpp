﻿

#include "dis/udis86.h"
#include <iostream>
#include <Windows.h>

int printValues(const char* filename) {
	const int MAX_FILEPATH = 255;
	char fileName[MAX_FILEPATH] = { 0 };
	memcpy_s(&fileName, MAX_FILEPATH, filename, MAX_FILEPATH);
	HANDLE file = NULL;
	DWORD fileSize = NULL;
	DWORD bytesRead = NULL;
	LPVOID fileData = NULL;
	PIMAGE_DOS_HEADER dosHeader = {};
	PIMAGE_NT_HEADERS imageNTHeaders = NULL;
	PIMAGE_SECTION_HEADER sectionHeader = {};
	PIMAGE_SECTION_HEADER importSection = {};
	IMAGE_IMPORT_DESCRIPTOR* importDescriptor = {};
	PIMAGE_THUNK_DATA thunkData = {};
	DWORD64 thunk = NULL;
	DWORD64 rawOffset = NULL;

	// open file
	file = CreateFileA(fileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (file == INVALID_HANDLE_VALUE) printf("Could not read file");

	// allocate heap
	fileSize = GetFileSize(file, NULL);
	fileData = HeapAlloc(GetProcessHeap(), 0, fileSize);

	// read file bytes to memory
	ReadFile(file, fileData, fileSize, &bytesRead, NULL);

	// IMAGE_DOS_HEADER
	dosHeader = (PIMAGE_DOS_HEADER)fileData;

	// IMAGE_NT_HEADERS
	auto bla = ((DWORD64)fileData + dosHeader->e_lfanew);
	imageNTHeaders = (PIMAGE_NT_HEADERS)bla;

	// get offset to first section headeer
	DWORD64 sectionLocation = (DWORD64)imageNTHeaders + sizeof(DWORD) + (DWORD64)(sizeof(IMAGE_FILE_HEADER)) + (DWORD64)imageNTHeaders->FileHeader.SizeOfOptionalHeader;
	DWORD64 sectionSize = (DWORD64)sizeof(IMAGE_SECTION_HEADER);

	// get offset to the import directory RVA
	DWORD64 importDirectoryRVA = imageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress;

	auto dwImportDirectoryVA = imageNTHeaders->OptionalHeader.DataDirectory[1].VirtualAddress;
	auto pSectionHeader = (PIMAGE_SECTION_HEADER)((DWORD64)imageNTHeaders + sizeof(IMAGE_NT_HEADERS));
	auto dwSectionCount = imageNTHeaders->FileHeader.NumberOfSections;

	for (DWORD64 dwSection = 0; dwSection < dwSectionCount && pSectionHeader->VirtualAddress <= dwImportDirectoryVA; pSectionHeader++, dwSection++);
	pSectionHeader--;
	auto dwRawOffset = (DWORD64)fileData + pSectionHeader->PointerToRawData;
	auto pImportDescriptor = (PIMAGE_IMPORT_DESCRIPTOR)(dwRawOffset + (dwImportDirectoryVA - pSectionHeader->VirtualAddress));
	for (; pImportDescriptor->Name != 0; pImportDescriptor++)
	{
		printf("\nDLL Name : %s\n\n", dwRawOffset + (pImportDescriptor->Name - pSectionHeader->VirtualAddress));
		auto pThunkData = (PIMAGE_THUNK_DATA)(dwRawOffset + (pImportDescriptor->FirstThunk - pSectionHeader->VirtualAddress));
		for (; pThunkData->u1.AddressOfData != 0; pThunkData++)
			printf("\tFunction : %s\n", (dwRawOffset + (pThunkData->u1.AddressOfData - pSectionHeader->VirtualAddress + 2)));
	}

	// print section data
	for (int i = 0; i < imageNTHeaders->FileHeader.NumberOfSections; i++) {
		sectionHeader = (PIMAGE_SECTION_HEADER)sectionLocation;
		printf("\t%s\n", sectionHeader->Name);
		printf("\t\t0x%x\t\tVirtual Size\n", sectionHeader->Misc.VirtualSize);
		printf("\t\t0x%x\t\tVirtual Address\n", sectionHeader->VirtualAddress);
		printf("\t\t0x%x\t\tSize Of Raw Data\n", sectionHeader->SizeOfRawData);
		printf("\t\t0x%x\t\tPointer To Raw Data\n", sectionHeader->PointerToRawData);
		printf("\t\t0x%x\t\tPointer To Relocations\n", sectionHeader->PointerToRelocations);
		printf("\t\t0x%x\t\tPointer To Line Numbers\n", sectionHeader->PointerToLinenumbers);
		printf("\t\t0x%x\t\tNumber Of Relocations\n", sectionHeader->NumberOfRelocations);
		printf("\t\t0x%x\t\tNumber Of Line Numbers\n", sectionHeader->NumberOfLinenumbers);
		printf("\t\t0x%x\tCharacteristics\n", sectionHeader->Characteristics);

		// save section that contains import directory table
		if (importDirectoryRVA >= sectionHeader->VirtualAddress && importDirectoryRVA < sectionHeader->VirtualAddress + sectionHeader->Misc.VirtualSize) {
			importSection = sectionHeader;
		}
		sectionLocation += sectionSize;
	}

	// get file offset to import table
	rawOffset = (DWORD64)fileData + importSection->PointerToRawData;

	// get pointer to import descriptor's file offset. Note that the formula for calculating file offset is: imageBaseAddress + pointerToRawDataOfTheSectionContainingRVAofInterest + (RVAofInterest - SectionContainingRVAofInterest.VirtualAddress)
	importDescriptor = (PIMAGE_IMPORT_DESCRIPTOR)(rawOffset + (imageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress - importSection->VirtualAddress));

	return 0;

	//printf("\n******* DLL IMPORTS *******\n");
	//for (; importDescriptor->Name != 0; importDescriptor++) {
	//	// imported dll modules
	//	printf("\t%s\n", rawOffset + (importDescriptor->Name - importSection->VirtualAddress));
	//	thunk = importDescriptor->OriginalFirstThunk == 0 ? importDescriptor->FirstThunk : importDescriptor->OriginalFirstThunk;
	//	thunkData = (PIMAGE_THUNK_DATA)(rawOffset + (thunk - importSection->VirtualAddress));
	//
	//	// dll exported functions
	//	for (; thunkData->u1.AddressOfData != 0; thunkData++) {
	//		//a cheap and probably non-reliable way of checking if the function is imported via its ordinal number ¯\_(ツ)_/¯
	//		if (thunkData->u1.AddressOfData > 0x80000000) {
	//			//show lower bits of the value to get the ordinal ¯\_(ツ)_/¯
	//			printf("\t\tOrdinal: %x\n", (WORD)thunkData->u1.AddressOfData);
	//		}
	//		else {
	//			printf("\t\t%s\n", (rawOffset + (thunkData->u1.AddressOfData - importSection->VirtualAddress + 2)));
	//		}
	//	}
	//}

	return 0;
}

DATA_BLOB getFile(const char* name)
{
	DWORD bytesRead;
	auto file = CreateFileA(name, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (file == INVALID_HANDLE_VALUE) printf("Could not read file");

	// allocate heap
	auto fileSize = GetFileSize(file, NULL);
	auto fileData = malloc(fileSize);

	// read file bytes to memory
	ReadFile(file, fileData, fileSize, &bytesRead, NULL);

	DATA_BLOB ret;
	ret.pbData = (BYTE*)fileData;
	ret.cbData = fileSize;

	return ret;
}

DATA_BLOB getEntryPoint(DATA_BLOB& file)
{
	auto dosHeader = (PIMAGE_DOS_HEADER)file.pbData;
	auto imageNTHeaders = (PIMAGE_NT_HEADERS)((DWORD64)file.pbData + dosHeader->e_lfanew);

	DATA_BLOB ret;
	ret.pbData = (BYTE*)((DWORD64)file.pbData + imageNTHeaders->OptionalHeader.BaseOfCode);
	ret.cbData = imageNTHeaders->OptionalHeader.SizeOfCode;

	return ret;
}

int main()
{


	DATA_BLOB buff = getFile("ConsoleApplication1.exe");
	DATA_BLOB code = getEntryPoint(buff);

    ud_t ud_obj;

	printValues("ConsoleApplication1.exe");
    FILE* file = fopen("ConsoleApplication1.exe", "rb");

    //ImageFileType(&hfile);
    ud_init(&ud_obj);
    ud_set_input_file(&ud_obj, file);
    ud_set_mode(&ud_obj, 64);
    ud_set_syntax(&ud_obj, UD_SYN_INTEL);
    std::string data = "";
    while (ud_disassemble(&ud_obj)) {
        data += ud_insn_asm(&ud_obj);
        data += '\n';
    }

    std::cout << data << std::endl;

    std::cout << data.find("call") << " " << std::string::npos << std::endl;
    fclose(file);

	return 0;
}