import pefile
#from binarytree import Node
import sys
import re
import types
import time

#sys.setrecursionlimit(2147483647)
regs = ['rax', 'eax', 'ax', 'al', 'rbx', 'ebx', 'bx', 'bl', 'rcx', 'ecx', 'cx', 'cl', 'rdx', 'edx', 'dx', 'dl', 'rsi', 'esi', 'si', 'sil', 'rdi', 'edi', 'di', 'dil', 'rbp', 'ebp', 'bp', 'bpl', 'rsp', 'esp', 'sp', 'spl', 'r8', 'r8d',
        'r8w', 'r8b', 'r9', 'r9d', 'r9w', 'r9b', 'r10', 'r10d', 'r10w', 'r10b', 'r11', 'r11d', 'r11w', 'r11b', 'r12', 'r12d', 'r12w', 'r12b', 'r13', 'r13d', 'r13w', 'r13b', 'r14', 'r14d', 'r14w', 'r14b', 'r15', 'r15d', 'r15w', 'r15b']


class Command:
    def __init__(self, address, opcode, val, isCall, isRet=False):
        self.addr = address
        self.opcode = opcode
        self.val = val
        self.isCall = isCall
        self.visited = False
        self.isRet = isRet



def cleanValue(val):
    ret = re.findall(r'([0-9A-F]+)', val, re.I)[-1]
    return ret

class Node:
    def __init__(self, value):
        self.value = value
        self.right = None
        self.left = None
        self.visited = False
        #self.commands = []
        self.endAddr = None
        self.external_commands = []
    
    
def recursiveBinarySearch(aList, target, start, end):
    #aList = sorted(aList)

    if end-start+1 <= 0:
        return False
    else:
        midpoint = start + (end - start) // 2
        if aList[midpoint] == target:
            return midpoint
        else:
            if target < aList[midpoint]:
                return recursiveBinarySearch(aList, target, start, midpoint-1)
            else:
                return recursiveBinarySearch(aList ,target, midpoint+1, end)


def handleNode(data, addr, nodesArr , data_keys, nameToAddrKeys, first= False):
    ret = Node(addr)


    if addr not in data or addr in nameToAddrKeys:
        #nodesArr.append(ret)
        nodesArr[addr] = ret
        return ret

    #data_keys = sorted(data.keys())
    #start = time.time()
    #data_index = data_keys.index(addr)
    data_index = recursiveBinarySearch(data_keys, addr, 0, len(data_keys))
    #end = time.time()
    #print(end - start)
    JustCallsIter= filter(lambda t: data[t[1]].isCall, enumerate(data_keys[data_index:]))
    #JustCalls = filter(lambda t: data[t[1]].isCall, enumerate(data_keys))

    for index, address in JustCallsIter:
        
        command = data[address]
        if command.visited:
            ret.endAddr = address
            ret = getNodeFromTree(command.addr, nodesArr)
            #nodesArr.append(ret)
            return ret
        command.visited = True
        if command.isRet:
            ret.endAddr = address
            nodesArr[address] = ret
            return ret

        
        if command.isCall:
            newAddr = cleanValue(command.val)
            if not newAddr in regs:
                newAddr = int(newAddr, base=16)
                if newAddr in data:
                    ret.endAddr = address
                    #next(x for x in JustCalls if x > newAddr)
                    #for i in itertools.ifilter(lambda x: x < 3, range(10))

                    ret.right = next(filter(lambda t: t >= newAddr and data[t].isCall, data_keys[recursiveBinarySearch(data_keys, newAddr, 0, len(data_keys)):]), None)
                    #ret.right = next(x for x in JustCalls if x[1] >= newAddr)[1]
                    ret.left = next(JustCallsIter, (None, None))[1]

                    
                    nodesArr[address] = ret
                    return ret
                else:
                    ret.external_commands.append(command)
                    
        nodesArr[address] = ret
        #    else:
        #        ret.commands.append(command)
        #else:
        #    ret.commands.append(command)

    return ret

def runOnTree(data, addr, nodesArr, data_keys, nameToAddrKeys):
    
    root = handleNode(data, addr, nodesArr, data_keys, nameToAddrKeys, True)

    stack = []

    curr = root
    stack.append(curr)

    while(len(stack) != 0):
        if not isinstance(curr.right, Node) and curr.right:
            try:
                curr.right = handleNode(data, curr.right, nodesArr, data_keys, nameToAddrKeys)
                curr = curr.right
                stack.append(curr)
            except StopIteration as e:
                curr.right = None
                curr = stack.pop()
        elif not isinstance(curr.left, Node) and curr.left:
            try:
                curr.left = handleNode(data, curr.left, nodesArr, data_keys, nameToAddrKeys)
                curr = curr.left
                stack.append(curr)
            except StopIteration as e:
                curr.left = None
                curr = stack.pop()

        else:
            curr = stack.pop()
        
    #print(root)
    return root
            

'''
def inOrder(root): 
      
    # Set current to root of binary tree 
    current = root  
    stack = [] # initialize stack 
    done = 0 
      
    while True: 
          
        # Reach the left most Node of the current Node 
        if current is not None: 
              
            # Place pointer to a tree node on the stack  
            # before traversing the node's left subtree 
            stack.append(current) 
          
            current = current.left  
  
          
        # BackTrack from the empty subtree and visit the Node 
        # at the top of the stack; however, if the stack is  
        # empty you are done 
        elif(stack): 
            current = stack.pop() 
            print(current.data, end=" ") # Python 3 printing 
          
            # We have visited the node and its left  
            # subtree. Now, it's right subtree's turn 
            current = current.right  
  
        else: 
            break
'''
       


    
'''
def newHandleNode(data, addr , dataDict):
    data_keys = sorted(dataDict.keys())
    node = Node(addr)
    for index , addr in enumerate(data_keys):
        command = data[addr]
        if command.visited:
            continue
        
        if command.isCall:
            command.visited = True
            
            newAddr = cleanValue(command.val)
            newAddress_index = data_keys.index(newAddr)
            
            node.right =  Node(newAddr)
            node = node.right

            for index , address in enumerate(data_keys[newAddress_index + 1:]):
                if data[address].isCall: break
                data[address].visited = True
                node.left =  Node(address)
                node = node.left
        else:
            command.visited = True
            node.left = Node(command.val)
            node = node.left


def nodeHandleNoRecur(data, entry):
    JustCalls = [x for x in filter(lambda t: data[t].isCall or data[t].isRet, sorted(data.keys()))]
    nodesArr = []
    parantsIndex = []
    parantsNode = []
    index = 0
    node = Node(entry)
    root = node

    while(index != len(JustCalls)):
        print(JustCalls[index])
        command = data[JustCalls[index]]
        if command.visited:
            #node.right = getNodeFromTree(data[JustCalls[index]], nodesArr)
            #node = node.right
            index = parantsIndex.pop()
            node = parantsNode.pop()
            index += 1
            continue

        command.visited = True

        if command.isRet:
            index = parantsIndex.pop()
            node = parantsNode.pop()
            index += 1
            continue


        newAddr = cleanValue(command.val)
        if newAddr in regs:
            continue

        newAddr = int(newAddr, base=16)
        if newAddr in data:
            if newAddr in JustCalls:
                index = JustCalls.index(newAddr)
            else:
                closest_value = min(JustCalls, key=lambda x:abs(x-newAddr))
                index = JustCalls.index(closest_value) if closest_value > newAddr else JustCalls.index(closest_value) + 1
            parantsIndex.append(index)
            parantsNode.append(node)
            node.right = Node(newAddr)
            nodesArr.append(node.right)
            node = node.right
        else:
            node.external_commands.append(newAddr)
        
        index += 1

    return root

'''
def getNodeFromTree(addr, nodesArr):
    '''
    start

    addr: lakjhsclasc
    end'''
    ret = nodesArr.get(addr, False)
    if ret:
        return ret


    arr = nodesArr.keys()
    arr.sort()
    arr.reverse()
    
    return next(nodesArr[x] for x in arr if x < addr)
    
    
    