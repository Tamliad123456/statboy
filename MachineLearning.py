import csv
import tensorflow as tf
import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import json
import logging
logging.getLogger().setLevel(logging.INFO)


vocab_size = 132974
embedding_dim = 64
max_length = 50
trunc_type = 'post'
padding_type = 'post'
oov_tok = '<OOV>'
training_portion = .8

def train(train_padded, training_label_seq, validation_label_seq, validation_padded):
    model = tf.keras.Sequential([
    # Add an Embedding layer expecting input vocab of size 5000, and output embedding dimension of size 64 we set at the top
    tf.keras.layers.Embedding(vocab_size, embedding_dim),
    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(embedding_dim)),
    #tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(32)),
    # use ReLU in place of tanh function since they are very good alternatives of each other.
    tf.keras.layers.Dense(embedding_dim, activation='relu'),
    # Add a Dense layer with 6 units and softmax activation.
    # When we have multiple outputs, softmax convert outputs layers into a probability distribution.
    tf.keras.layers.Dense(11, activation='softmax')
    ])
    print(model.summary())

    model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    num_epochs = 10
    history = model.fit(train_padded, training_label_seq, epochs=num_epochs, validation_data=(validation_padded, validation_label_seq), verbose=1)
    model.save('MLmodel.h5')

def prepareForTrain(values, labels):
    train_size = int(len(values) * training_portion)

    validation_labels = labels[train_size:]
    validation_sequences = values[train_size:]


    train_labels = labels[:train_size]
    train_sequences = values[:train_size]

    validation_padded = pad_sequences(validation_sequences, maxlen=max_length, padding=padding_type, truncating=trunc_type)

    training_label_seq = np.array(train_labels)
    validation_label_seq = np.array(validation_labels)

    train_padded = pad_sequences(train_sequences, maxlen=max_length, padding=padding_type, truncating=trunc_type)
    train(train_padded, training_label_seq, validation_label_seq, validation_padded)


reset = {
    "ransomware" : 0,
    "botnet" : 1,
    "dropper" : 2,
    "trojan" : 3, 
    "apt" : 4,
    "virus": 5,
    "worm": 6, 
    "rootkit": 7,
    "backdoor": 8,
    "exploit": 9,
    "bot": 10,
}

with open("data.json", "r") as JsonFile:
    data = json.load(JsonFile)

    values = list()
    labels = list()

    for x in data:
        for value in data[x]:
            values += value
            labels += [reset[x]] * len(value)

    prepareForTrain(values, labels)
