import re

def parseFile(path):
    
    content = open(path, "r").read()
    
    nodes_regex = r"node: ({[\S\s]*?})"
    edges_regex = r"node: ({[\S\s]*?})"
    
    nodes = re.findall(nodes_regex, content)
    edges = re.findall(edges_regex, content)
    
    return (
        map(lambda node: node.replace(r"([a-zA-Z0-9-]+):([a-zA-Z0-9-]+)", "\"$1\":\"$2\""), nodes), 
        map(lambda edge: edge.replace(r"([a-zA-Z0-9-]+):([a-zA-Z0-9-]+)", "\"$1\":\"$2\""), edges)
    )
    
def handleNode(node):
    header_regex = r"([a-zA-Z0-9-]+): \"([\S\s]*?)\""
    title = re.findall(header_regex, node)[0][1]
    content = re.findall(header_regex, node)[1][1]
    content_regex = r"([\S\s]+):([\S\s]+)"
    content = re.findall(content_regex, content)[0][1]
    return (title, content)