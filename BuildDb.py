import pefile
import os
path = 'c:\\windows\\'

functions = []

with open("Data.txt", "w") as DB:
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if file.endswith(".dll"):
                files.append(os.path.join(r, file))
    for dllfile in files:
        #print(dllfile)
        try:
            pe = pefile.PE(dllfile, fast_load=True)
            isDotNet = pe.OPTIONAL_HEADER.DATA_DIRECTORY[14]
            if isDotNet.VirtualAddress == 0 and isDotNet.Size == 0:
                pe = pefile.PE(dllfile)
                for exp in pe.DIRECTORY_ENTRY_EXPORT.symbols:
                    if exp.name not in functions:
                        functions.append(exp.name)
                        DB.write(exp.name.decode() + "\n")
                print(f"finished {dllfile}")
        except:
            pass