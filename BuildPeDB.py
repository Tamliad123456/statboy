import json
import os
import Parser

reset = {
    "ransomware" : [],
    "botnet" : [],
    "dropper" : [],
    "trojan" : [], 
    "apt" : [],
    "virus": [],
    "worm": [], 
    "rootkit": [],
    "backdoor": [],
    "exploit": [],
    "bot": [],
}

with open("data.json", "w") as JsonFile:
    json.dump(reset, JsonFile)


ends = ['.i64' , '.gz' , '.id1', '.id0', '.id2' , '.nam' , '.til']

for MalType in reset.keys():
    directory = os.fsencode(f"malwares\\{MalType}")
    for root, dirs, files in os.walk(directory):
        #print(root , dirs , files)
        for fileName in files:
            if fileName.decode()[fileName.decode().rfind("."):] not in ends:
                #print(os.path.join(root, fileName))
                #print(os.path.join(root, fileName))
                #paths = os.popen(f"python Parser.py {os.path.join(root, fileName).decode()}").read()
                paths = Parser.mainFunc(os.path.join(root, fileName).decode())
                print(paths)
                with open("data.json", "r+") as JsonFile:
                    parsed_json = (json.loads(JsonFile.read()))
                    parsed_json[MalType].append(paths)
                    JsonFile.seek(0)
                    json.dump(parsed_json, JsonFile)

        