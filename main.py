import os , Parser
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
from pprint import pprint
import sys
model = load_model("MLmodel.h5")

max_length = 50
trunc_type = 'post'
padding_type = 'post'

reset = {
    0: "ransomware",
    1: "botnet",
    2: "dropper" ,
    3: "trojan" , 
    4: "apt" ,
    5: "virus",
    6: "worm", 
    7: "rootkit",
    8: "backdoor",
    9: "exploit",
    10: "bot",
}


def mainFunc(filePath):
    if  not os.path.exists(filePath): return
    paths = Parser.mainFunc(filePath)
    padded = pad_sequences(paths, maxlen=max_length, padding=padding_type, truncating=trunc_type)
    num = np.argmax(model.predict(padded), axis=-1)
    num = np.argmax(np.bincount(num))
    print(reset[num])


if __name__ == "__main__":
    mainFunc(sys.argv[1])